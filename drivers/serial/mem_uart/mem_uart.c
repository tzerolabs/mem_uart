#define DT_DRV_COMPAT tzero_mem_uart

/**
 * @file
 * @brief memory uart controller
 */

#include <drivers/uart.h>
#include <sys/ring_buffer.h>
#define LOG_LEVEL CONFIG_LED_LOG_LEVEL
#include <logging/log.h>

LOG_MODULE_REGISTER(tzero_mem_uart);

struct uart_mem_config {
	uint32_t Config;

};

extern struct ring_buf  uart_mem_0__pipe_out;
extern struct ring_buf  uart_mem_0__pipe_in;

struct uart_mem_data {
#ifdef CONFIG_UART_ASYNC_API
	uart_callback_t callback;
	void *user_data;
#endif /* CONFIG_UART_ASYNC_API */
	struct uart_config uart_config;

};


static inline
const struct uart_mem_config *get_dev_config(const struct device *dev)
{
	return dev->config;
}

static inline
struct uart_mem_data *get_dev_data(const struct device *dev)
{
	return (struct uart_mem_data *)dev->data;
}

static int uart_mem_init(const struct device *dev)
{
	return 0;
}

/**
 * @brief Poll the device for input.
 *
 * @param dev UART device struct
 * @param c Pointer to character
 *
 * @return 0 if a character arrived, -1 if the input buffer if empty.
 */

static int uart_mem_poll_in(const struct device *dev, unsigned char *c)
{
	if(!k_is_pre_kernel())
	{
		if(ring_buf_is_empty(&uart_mem_0__pipe_in) == 0)
		{
				ring_buf_get(&uart_mem_0__pipe_in,c,1);
				return 0;
		}
	}

	return -1;
}

/**
 * @brief Output a character in polled mode.
 *
 * @param dev UART device struct
 * @param c Character to send
 */


static void uart_mem_poll_out(const struct device *dev, unsigned char c)
{
	if(!k_is_pre_kernel())
	{
		ring_buf_put(&uart_mem_0__pipe_out,&c,1);
	}
}


/** Console I/O function */
static int uart_mem_err_check(const struct device *dev)
{
	/* register bitfields maps to the defines in uart.h */
	return 0;
}

static int uart_mem_configure(const struct device *dev,
			      			 const struct uart_config *cfg)
{

	get_dev_data(dev)->uart_config = *cfg;

	return 0;
}

static int uart_mem_config_get(const struct device *dev,
				struct uart_config *cfg)
{
	*cfg = get_dev_data(dev)->uart_config;
	return 0;
}

static const struct uart_driver_api uart_mem_driver_api = {
	.poll_in = uart_mem_poll_in,
	.poll_out = uart_mem_poll_out,
     .err_check        = uart_mem_err_check,
	.configure        = uart_mem_configure,
	.config_get       = uart_mem_config_get,

};

#define UART_MEM(idx)                   DT_NODELABEL(mem_uart##idx)
#define UART_MEM_PROP(idx, prop)        DT_PROP(UART_MEM(idx), prop)
#define UART_MEM_CONFIG_NAME(idx)       uart_mem##idx##_config
#define UART_MEM_DATA_NAME(idx)         uart_mem##idx##_data


#define UART_MEM_CONFIG(idx)						    \
										\
									    \
	static struct uart_mem_config UART_MEM_CONFIG_NAME(idx) = { \
	.Config=0,\
	}

#define UART_MEM_DATA(idx)						   	    \
	\
	static struct uart_mem_data UART_MEM_DATA_NAME(idx) = { \
	}

#define UART_MEM_INIT(idx, config)					      \
	UART_MEM_DATA(idx);		  							  \
									     							\
	DEVICE_DT_DEFINE(UART_MEM(idx), uart_mem_init, NULL, \
			    &uart_mem##idx##_data, config,		      \
			    PRE_KERNEL_1, CONFIG_KERNEL_INIT_PRIORITY_DEVICE, \
			    &uart_mem_driver_api)


#ifdef CONFIG_MEM_UART_0

	RING_BUF_DECLARE(uart_mem_0__pipe_out, UART_MEM_PROP(0, tx_buffer_size));
	RING_BUF_DECLARE(uart_mem_0__pipe_in, UART_MEM_PROP(0, rx_buffer_size));
	UART_MEM_CONFIG(0);

	UART_MEM_INIT(0, &UART_MEM_CONFIG_NAME(0));
#endif

