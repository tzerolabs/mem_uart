set (MEM_UART_DIR ${CMAKE_CURRENT_LIST_DIR} CACHE PATH "mem_uart root directory")

add_subdirectory(drivers/serial)
